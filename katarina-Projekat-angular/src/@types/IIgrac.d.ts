declare interface IIgrac {
  id: number;
  ime: string;
  prezime: string;
  jmbg: string;
  pozicija: string;
  datumRodj: Date;
  mesto: IMesto;
  klub: IKlub;
}
