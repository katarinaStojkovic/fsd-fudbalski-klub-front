declare interface IUtakmica {
  id: number;
  datumOdigravanja: Date;
  domacinId: IKlub;
  gostId: IKlub;
  takmicenjeId: ITakmicenje;
}
