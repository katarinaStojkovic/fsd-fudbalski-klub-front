import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from '../material.module';
import {HttpClientModule} from '@angular/common/http';
import { AdminComponent } from './pages/admin/admin.component';
import {PlayerComponent} from './pages/player/player.component';
import {PlayerListComponent} from './components/player/player-list/player-list.component';
import { PlayerDialogComponent } from './components/player/player-list/player-dialog/player-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MestaComponent} from './components/player/mesta/mesta.component';
import {MestaDialogComponent} from './components/player/mesta/mesta-dialog/mesta-dialog.component';
import {KlubComponent} from './components/player/klub/klub.component';
import {KlubDialogComponent} from './components/player/klub/klub-dialog/klub-dialog.component';
import {UtakmicaComponent} from './components/player/utakmica/utakmica.component';
import {UtakmicaDialogComponent} from './components/player/utakmica/utakmica-dialog/utakmica-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    PlayerComponent,
    PlayerListComponent,
    PlayerDialogComponent,
    MestaComponent,
    MestaDialogComponent,
    KlubComponent,
    KlubDialogComponent,
    UtakmicaComponent,
    UtakmicaDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
