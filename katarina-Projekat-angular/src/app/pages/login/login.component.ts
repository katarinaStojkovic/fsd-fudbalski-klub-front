import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../util/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthenticationService) {
  }

  ngOnInit(): void {
  }

  login(username: string, password: string): void {
    this.auth.login(username, password);
  }
}
