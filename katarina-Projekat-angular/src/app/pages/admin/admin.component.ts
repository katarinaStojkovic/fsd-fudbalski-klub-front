import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../util/authentication.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  menu = 'players';

  constructor(private auth: AuthenticationService) {
  }

  ngOnInit(): void {
  }

  logOut(): void {
    this.auth.logOut();
  }
}
