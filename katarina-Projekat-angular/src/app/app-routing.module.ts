import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {PlayerComponent} from './pages/player/player.component';
import {PlayerAuthGuardService} from './util/auth-guards/player-auth-guard';
import {AdminAuthGuardService} from './util/auth-guards/admin-auth-guard';
import {AdminComponent} from './pages/admin/admin.component';

const routes: Routes = [
  {
    path: 'player',
    component: PlayerComponent,
    canActivate: [PlayerAuthGuardService]
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AdminAuthGuardService]
  },
  {
    path: '**',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
