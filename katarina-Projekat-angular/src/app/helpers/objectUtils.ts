import {FormGroup} from '@angular/forms';

export const compareObjectsById = (obj1: any, obj2: any) => {
  return obj1?.id === obj2?.id;
};

export const setFormFields = (formGroup: FormGroup, object: any) => {
  if (!formGroup || !object) {
    return;
  }
  for (const [key, value] of Object.entries(object)) {
    formGroup.get(key)?.setValue(value);
  }
};

// returns value from object property relative path
export const getPropertyValue = (item: any, property: string) => {
  const parts = property.split('.');
  let value = item[parts[0]];
  for (let i = 1; i < parts.length; i++) {
    value = value[parts[i]];
  }
  return value;
};

export const getCurrentNumberValue = (value: string): number => {
  return +value.replace(',', '.');
};

export const handleNumberKeyDown = (event: KeyboardEvent) => {
  if (!['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ',', '.', 'Backspace', 'Delete'].includes(event.key)) { event.preventDefault(); }
};
