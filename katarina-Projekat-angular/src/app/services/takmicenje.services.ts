import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TakmicenjeService {

  constructor(private http: HttpClient) {
  }

  getAllTakmicenja(): Promise<ITakmicenje[]> {
    return this.http.get<ITakmicenje[]>(`${environment.api}/takmicenjes`, {
      responseType: 'json'
    }).toPromise();
  }

  saveTakmicenja(takmicenje: ITakmicenje): Promise<ITakmicenje> {
    return this.http.post<ITakmicenje>(`${environment.api}/takmicenjes`, takmicenje, {responseType: 'json'}).toPromise();
  }

  updateTakmicenja(takmicenje: ITakmicenje): Promise<ITakmicenje> {
    return this.http.put<ITakmicenje>(`${environment.api}/takmicenjes`, takmicenje, {responseType: 'json'}).toPromise();
  }

  deleteTakmicenja(ID: number): Promise<any> {
    return this.http.delete(`${environment.api}/takmicenjes/${ID}`, {responseType: 'json'}).toPromise();
  }
}
