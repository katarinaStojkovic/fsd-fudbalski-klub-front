import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) {
  }

  getAllplayers(): Promise<IIgrac[]> {
    return this.http.get<IIgrac[]>(`${environment.api}/igracs`, {
      responseType: 'json'
    }).toPromise();
  }

  saveIgrac(player: IIgrac): Promise<IIgrac> {
    return this.http.post<IIgrac>(`${environment.api}/igracs`, player, {responseType: 'json'}).toPromise();
  }

  updateIgrac(player: IIgrac): Promise<IIgrac> {
    return this.http.put<IIgrac>(`${environment.api}/igracs`, player, {responseType: 'json'}).toPromise();
  }
}
