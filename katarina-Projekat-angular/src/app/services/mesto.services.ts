import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MestoService {

  constructor(private http: HttpClient) {
  }

  getAllMesta(): Promise<IMesto[]> {
    return this.http.get<IMesto[]>(`${environment.api}/mestos`, {
      responseType: 'json'
    }).toPromise();
  }

  saveMesto(mesto: IMesto): Promise<IMesto> {
    return this.http.post<IMesto>(`${environment.api}/mestos`, mesto, {responseType: 'json'}).toPromise();
  }

  updateMesto(mesto: IMesto): Promise<IMesto> {
    return this.http.put<IMesto>(`${environment.api}/mestos`, mesto, {responseType: 'json'}).toPromise();
  }

  deleteMesto(ID: number): Promise<any> {
    return this.http.delete(`${environment.api}/mestos/${ID}`, {responseType: 'json'}).toPromise();
  }
}
