import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtakmicaService {

  constructor(private http: HttpClient) {
  }

  getAllUtakmica(): Promise<IUtakmica[]> {
    return this.http.get<IUtakmica[]>(`${environment.api}/utakmicas`, {
      responseType: 'json'
    }).toPromise();
  }

  saveUtakmica(utakmica: IUtakmica): Promise<IUtakmica> {
    return this.http.post<IUtakmica>(`${environment.api}/utakmicas`, utakmica, {responseType: 'json'}).toPromise();
  }

  updateUtakmica(utakmica: IUtakmica): Promise<IUtakmica> {
    return this.http.put<IUtakmica>(`${environment.api}/utakmicas`, utakmica, {responseType: 'json'}).toPromise();
  }

  deleteUtakmica(ID: number): Promise<any> {
    return this.http.delete(`${environment.api}/utakmicas/${ID}`, {responseType: 'json'}).toPromise();
  }
}
