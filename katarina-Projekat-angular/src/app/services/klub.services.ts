import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class KlubService {

  constructor(private http: HttpClient) {
  }

  getAllKlubs(): Promise<IKlub[]> {
    return this.http.get<IKlub[]>(`${environment.api}/klubs`, {
      responseType: 'json'
    }).toPromise();
  }

  saveKlub(klub: IKlub): Promise<IKlub> {
    return this.http.post<IKlub>(`${environment.api}/klubs`, klub, {responseType: 'json'}).toPromise();
  }

  updateKlub(klub: IKlub): Promise<IKlub> {
    return this.http.put<IKlub>(`${environment.api}/klubs`, klub, {responseType: 'json'}).toPromise();
  }

  deleteKlub(ID: number): Promise<any> {
    return this.http.delete(`${environment.api}/klubs/${ID}`, {responseType: 'json'}).toPromise();
  }
}
