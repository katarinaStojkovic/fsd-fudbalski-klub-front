import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import { tap } from 'rxjs/operators';
import { SnackbarService } from './snackbar-handler';
import {AuthenticationService} from './authentication.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  private readonly headerPrefix = 'Bearer ';

  constructor(private authenticationService: AuthenticationService, private snackService: SnackbarService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.search('login') === -1) {
      const token = (req.url.search('admin') >= 0) ? atob(sessionStorage.getItem('acc_a')) :  atob(sessionStorage.getItem('acc_t'));
      const authHeader = this.headerPrefix + token;
      req = req.clone({
        setHeaders: {
          Authorization: authHeader
        }
      });
    }


    return next.handle(req).pipe(
      tap(succ => {
        const status = (succ as any).status;
        if (status && status === 200 && !req.url.includes('login')) {
          switch (req.method) {
            case 'POST':
              this.snackService.showSuccessSnackbar('Uspešno sačuvano!');
              break;
            case 'PUT':
            case 'PATCH':
              this.snackService.showSuccessSnackbar('Uspešno izmenjeno!');
              break;
            case 'DELETE':
              this.snackService.showSuccessSnackbar('Uspešno obrisano!');
              break;
          }
        }
      }, err => {
        if (err.status === 401 || err.status === 403) {
          this.authenticationService.logOut();
        }
        if (err.status === 0) {
          this.snackService.showErrorSnackbar(`Server trenutno nije dostupan. Pokušajte kasnije, hvala.`);
        } else {
          this.snackService.showErrorSnackbar(err.status + ': ' + (err.error.error || err.error));
        }
      }),
    );
  }
}
