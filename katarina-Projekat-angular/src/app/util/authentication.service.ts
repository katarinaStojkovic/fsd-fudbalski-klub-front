import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import jwt_decode from 'jwt-decode';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../components/models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public currentUser: Observable<User | null>;
  token: any;
  private readonly currentUserSubject: BehaviorSubject<User | null>;

  constructor(private http: HttpClient, private route: Router) {
    const localStorageCurrentUser = localStorage.getItem('currentUser');
    let parsedCurrentUser: User | null = null;
    if (localStorageCurrentUser) {
      parsedCurrentUser = JSON.parse(localStorageCurrentUser);
    }

    this.currentUserSubject = new BehaviorSubject<User | null>(parsedCurrentUser);
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User | null {
    if (this.currentUserSubject) {
      return this.currentUserSubject.value;
    }
    return null;
  }

  // tslint:disable-next-line:typedef
  public login(username: string, password: string): void {
    this.http.post(`${environment.api}/login`, {username, password}, {responseType: 'text'}).toPromise().then(token => {
      sessionStorage.setItem('acc_t', btoa(token));
      this.token = jwt_decode(token);
      console.log(this.token);

      switch (this.token.roles[0]) {
        case 'ROLE_ADMIN':
          this.route.navigate(['admin']);
          break;
        case 'ROLE_IGRAC':
          this.route.navigate(['igrac']);
          break;
      }
    });
  }
  logOut(): void {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.route.navigate(['login']);
  }

  isAdmin(): boolean {
    return this.hasRole('ADMIN');
  }

  isAuthenticated(): boolean {
    return !!this.currentUserValue;
  }

  hasRole(role: string): boolean {
    if (!role.startsWith('ROLE_')) {
      role = 'ROLE_' + role;
    }

    return this.getRoles().includes(role);
  }

  getRoles(): string[] {
    if (!this.currentUser) {
      return [];
    }

    // tslint:disable-next-line:no-non-null-assertion
    return this.currentUserValue!.roles;
  }

  hasAnyRole(roles: string[]): boolean {
    return roles.some(r => this.hasRole(r));
  }

}
