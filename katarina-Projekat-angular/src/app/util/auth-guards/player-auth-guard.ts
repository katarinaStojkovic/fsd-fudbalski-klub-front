import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthenticationService} from '../authentication.service';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlayerAuthGuardService implements CanActivate {

  constructor(private router: Router, private service: AuthenticationService) {
  }

  // tslint:disable-next-line:typedef
  canActivate() {
    if (!environment.production) {
      return true;
    }

    if (this.service.hasRole('ROLE_IGRAC')) {
      return true;
    } else {
      this.service.logOut();
      return false;
    }

  }
}
