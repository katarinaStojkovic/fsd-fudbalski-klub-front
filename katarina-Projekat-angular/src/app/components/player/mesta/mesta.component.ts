import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MestaDialogComponent} from './mesta-dialog/mesta-dialog.component';
import {AuthenticationService} from '../../../util/authentication.service';
import {MestoService} from '../../../services/mesto.services';

@Component({
  selector: 'app-mesta',
  templateUrl: './mesta.component.html',
  styleUrls: ['./mesta.component.scss']
})
export class MestaComponent implements OnInit {
  dataSourceMesta: any;
  mestaList: IMesto[];
  displayedColumns: string[] = ['ptt', 'naziv', 'option'];
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private service: MestoService, private authService: AuthenticationService, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.getAllMesta();
  }

  getAllMesta(): void {
    this.service.getAllMesta().then(data => {
      if (data) {
        this.mestaList = data;
        this.dataSourceMesta = new MatTableDataSource<any>(this.mestaList);
        this.dataSourceMesta.paginator = this.paginator;
        this.dataSourceMesta.sort = this.sort;
      }
    });
  }

  openMestaDialog(mesto?: IMesto): void {
    const dialogConfig = new MatDialogConfig();
    if (mesto) {
      dialogConfig.data = {
        mesto
      };
    }
    dialogConfig.autoFocus = false;
    dialogConfig.width = '30%';
    this.dialog.open(MestaDialogComponent, dialogConfig).afterClosed().subscribe(() => {
      this.getAllMesta();
    });
  }

  get hasPermission(): boolean {
    console.log(this.authService.hasRole('ROLE_ADMIN'));
    return this.authService.hasRole('ROLE_ADMIN');
  }

  deleteMesto(id: number): void {
    this.service.deleteMesto(id).then(() => {
      this.getAllMesta();
    });
  }
}
