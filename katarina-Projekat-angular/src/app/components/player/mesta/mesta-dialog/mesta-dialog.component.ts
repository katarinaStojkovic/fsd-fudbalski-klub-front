import {Component, Inject, OnInit} from '@angular/core';
import {MestoService} from '../../../../services/mesto.services';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {compareObjectsById, setFormFields} from '../../../../helpers/objectUtils';

@Component({
  selector: 'app-mesta-dialog',
  templateUrl: './mesta-dialog.component.html',
  styleUrls: ['./mesta-dialog.component.scss']
})
export class MestaDialogComponent implements OnInit {
  mesto: IMesto;
  form = new FormGroup({
    ptt: new FormControl('', Validators.required),
    naziv: new FormControl('', Validators.required),
    id: new FormControl(''),
  });

  constructor(private service: MestoService, @Inject(MAT_DIALOG_DATA) data: any,
              private dialogRef: MatDialogRef<MestaDialogComponent>) {
    if (data) {
      this.mesto = data.mesto;
      setFormFields(this.form, this.mesto);
    }
  }

  ngOnInit(): void {
  }

  saveMesto(): void {
    if (this.mesto) {
      this.service.updateMesto(this.form.value).then(() => this.close());
    } else {
      this.service.saveMesto(this.form.value).then(() => this.close());
    }
  }

  compareIds(obj1: any, obj2: any): boolean {
    return compareObjectsById(obj1, obj2);
  }

  close(): void {
    this.dialogRef.close(true);
  }
}
