import {Component, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {PlayerDialogComponent} from './player-dialog/player-dialog.component';
import {AdminService} from '../../../services/admin.services';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.scss']
})
export class PlayerListComponent implements OnInit {

  players: IIgrac[] = [];

  constructor(private dialog: MatDialog, private service: AdminService) {
  }

  ngOnInit(): void {
  }

  getAllPlayers(): void {
    this.service.getAllplayers().then(data => {
      this.players = data;
    });
  }

  playerDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '30%';
    // dialogConfig.data = data;

    const dialogRef = this.dialog.open(PlayerDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.getAllPlayers();
      }
    });
  }
}
