import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AdminService} from '../../../../services/admin.services';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-player-dialog',
  templateUrl: './player-dialog.component.html',
  styleUrls: ['./player-dialog.component.scss']
})
export class PlayerDialogComponent implements OnInit {
  private player: IIgrac;

  form = new FormGroup({
    ime: new FormControl(null, Validators.required),
    prezime: new FormControl(null, Validators.required),
    jmbg: new FormControl(null, Validators.required),
    pozicija: new FormControl(null, Validators.required),
    datumRodj: new FormControl(null, Validators.required),
    // mesto: new FormControl(null, Validators.required),
    // klub: new FormControl(null, Validators.required),
    slika: new FormControl(null, Validators.required),
  });

  constructor(private service: AdminService, @Inject(MAT_DIALOG_DATA)data: any, private dialogRef: MatDialogRef<PlayerDialogComponent>) {
    if (data !== undefined) {
      console.log('DATA', data);
      this.player = data;
    }
  }

  ngOnInit(): void {
  }

  save(): void {
    console.log(this.form.value);

    if (this.player?.id) {
      this.service.updateIgrac(this.form.value).then(() => this.close());
    } else {
      this.service.saveIgrac(this.form.value).then(() => this.close());
    }
  }

  close(): void {
    this.dialogRef.close(true);
  }

}
