import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {KlubDialogComponent} from './klub-dialog/klub-dialog.component';
import {AuthenticationService} from '../../../util/authentication.service';
import {MestoService} from '../../../services/mesto.services';
import {KlubService} from '../../../services/klub.services';

@Component({
  selector: 'app-klub',
  templateUrl: './klub.component.html',
  styleUrls: ['./klub.component.scss']
})
export class KlubComponent implements OnInit {
  dataSourceKlub: any;
  klubList: IKlub[];
  displayedColumns: string[] = ['naziv', 'mestoId.naziv', 'option'];
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private service: KlubService, private authService: AuthenticationService, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.getAllKlubs();
  }

  getAllKlubs(): void {
    this.service.getAllKlubs().then(data => {
      if (data) {
        this.klubList = data;
        this.dataSourceKlub = new MatTableDataSource<any>(this.klubList);
        this.dataSourceKlub.paginator = this.paginator;
        this.dataSourceKlub.sort = this.sort;
      }
    });
  }

  openKlubDialog(klub?: IKlub): void {
    const dialogConfig = new MatDialogConfig();
    if (klub) {
      dialogConfig.data = {
        klub
      };
    }
    dialogConfig.autoFocus = false;
    dialogConfig.width = '30%';
    this.dialog.open(KlubDialogComponent, dialogConfig).afterClosed().subscribe(() => {
      this.getAllKlubs();
    });
  }

  get hasPermission(): boolean {
    console.log(this.authService.hasRole('ROLE_ADMIN'));
    return this.authService.hasRole('ROLE_ADMIN');
  }

  deleteKlub(id: number): void {
    this.service.deleteKlub(id).then(() => {
      this.getAllKlubs();
    });
  }
}
