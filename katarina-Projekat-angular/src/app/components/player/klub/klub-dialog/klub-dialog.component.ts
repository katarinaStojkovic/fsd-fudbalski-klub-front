import {Component, Inject, OnInit} from '@angular/core';
import {MestoService} from '../../../../services/mesto.services';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {compareObjectsById, setFormFields} from '../../../../helpers/objectUtils';
import {KlubService} from '../../../../services/klub.services';

@Component({
  selector: 'app-klub-dialog',
  templateUrl: './klub-dialog.component.html',
  styleUrls: ['./klub-dialog.component.scss']
})
export class KlubDialogComponent implements OnInit {
  klub: IKlub;
  mesta: IMesto[] = [];
  form = new FormGroup({
    mesto: new FormControl(null),
    naziv: new FormControl('', Validators.required),
    id: new FormControl(''),
  });

  constructor(private service: KlubService, private serviceMesto: MestoService, @Inject(MAT_DIALOG_DATA) data: any,
              private dialogRef: MatDialogRef<KlubDialogComponent>) {
    if (data) {
      this.klub = data.klub;
      setFormFields(this.form, this.klub);
    }
  }

  ngOnInit(): void {
    this.getAllMesta();
  }


  getAllMesta(): void {
    this.serviceMesto.getAllMesta().then(data => this.mesta = data);
  }
  saveKlub(): void {
    if (this.klub) {
      this.service.updateKlub(this.form.value).then(() => this.close());
    } else {
      this.service.saveKlub(this.form.value).then(() => this.close());
    }
  }

  compareIds(obj1: any, obj2: any): boolean {
    return compareObjectsById(obj1, obj2);
  }

  close(): void {
    this.dialogRef.close(true);
  }
}
