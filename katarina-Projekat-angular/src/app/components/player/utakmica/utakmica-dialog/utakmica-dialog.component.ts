import {Component, Inject, OnInit} from '@angular/core';
import {MestoService} from '../../../../services/mesto.services';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {compareObjectsById, setFormFields} from '../../../../helpers/objectUtils';
import {KlubService} from '../../../../services/klub.services';
import {UtakmicaService} from '../../../../services/utakmica.services';
import {TakmicenjeService} from '../../../../services/takmicenje.services';

@Component({
  selector: 'app-utakmica-dialog',
  templateUrl: './utakmica-dialog.component.html',
  styleUrls: ['./utakmica-dialog.component.scss']
})
export class UtakmicaDialogComponent implements OnInit {
  utakmica: IUtakmica;
  klubovi: IKlub[] = [];
  takmicenja: ITakmicenje[] = [];
  form = new FormGroup({
    datumOdigravanja: new FormControl('', Validators.required),
    domacinId: new FormControl(null),
    gostId: new FormControl(null),
    takmicenjeId: new FormControl(null),
    id: new FormControl(''),
  });

  constructor(private service: UtakmicaService,
              private serviceKlub: KlubService,
              private serviceTakmicenje: TakmicenjeService,
              @Inject(MAT_DIALOG_DATA) data: any,
              private dialogRef: MatDialogRef<UtakmicaDialogComponent>) {
    if (data) {
      this.utakmica = data.utakmica;
      setFormFields(this.form, this.utakmica);
    }
  }

  ngOnInit(): void {
    this.getAllKlubovi();
    this.getAllTakmicenja();
  }
  getAllKlubovi(): void {
    this.serviceKlub.getAllKlubs().then(data => this.klubovi = data);
  }
  getAllTakmicenja(): void {
    this.serviceTakmicenje.getAllTakmicenja().then(data => this.takmicenja = data);
  }
  saveUtakmica(): void {
    if (this.utakmica) {
      this.service.updateUtakmica(this.form.value).then(() => this.close());
    } else {
      this.service.saveUtakmica(this.form.value).then(() => this.close());
    }
  }

  compareIds(obj1: any, obj2: any): boolean {
    return compareObjectsById(obj1, obj2);
  }

  close(): void {
    this.dialogRef.close(true);
  }
}
