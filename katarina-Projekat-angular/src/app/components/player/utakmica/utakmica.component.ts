import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {UtakmicaDialogComponent} from './utakmica-dialog/utakmica-dialog.component';
import {AuthenticationService} from '../../../util/authentication.service';
import {MestoService} from '../../../services/mesto.services';
import {KlubService} from '../../../services/klub.services';
import {UtakmicaService} from '../../../services/utakmica.services';

@Component({
  selector: 'app-utakmica',
  templateUrl: './utakmica.component.html',
  styleUrls: ['./utakmica.component.scss']
})
export class UtakmicaComponent implements OnInit {
  dataSourceUtakmica: any;
  utakmicaList: IUtakmica[];
  displayedColumns: string[] = ['datumOdigravanja', 'klub.domacin', 'klub.gost', 'takmicenje.nazivTakmicenja', 'option'];
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private service: UtakmicaService, private authService: AuthenticationService, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.getAllUtakmice();
  }

  getAllUtakmice(): void {
    this.service.getAllUtakmica().then(data => {
      if (data) {
        this.utakmicaList = data;
        this.dataSourceUtakmica = new MatTableDataSource<any>(this.utakmicaList);
        this.dataSourceUtakmica.paginator = this.paginator;
        this.dataSourceUtakmica.sort = this.sort;
      }
    });
  }

  openUtakmicaDialog(utakmica?: IUtakmica): void {
    const dialogConfig = new MatDialogConfig();
    if (utakmica) {
      dialogConfig.data = {
        utakmica
      };
    }
    dialogConfig.autoFocus = false;
    dialogConfig.width = '30%';
    this.dialog.open(UtakmicaDialogComponent, dialogConfig).afterClosed().subscribe(() => {
      this.getAllUtakmice();
    });
  }

  get hasPermission(): boolean {
    console.log(this.authService.hasRole('ROLE_ADMIN'));
    return this.authService.hasRole('ROLE_ADMIN');
  }

  deleteUtakmica(id: number): void {
    this.service.deleteUtakmica(id).then(() => {
      this.getAllUtakmice();
    });
  }
}
