export class MenuLinks {
  links: MenuLink[];

  constructor(links: IMenuLink[]) {
    this.links = links.map(m => new MenuLink(m));
  }
}

declare interface IMenuLink {
  displayValue: string;
  page: string | number;
  icon: string;
}

export class MenuLink implements IMenuLink {
  displayValue: string;
  page: string | number;
  icon: string;

  constructor({displayValue, page, icon}: IMenuLink) {
    this.displayValue = displayValue;
    this.page = page;
    this.icon = icon;
  }
}
