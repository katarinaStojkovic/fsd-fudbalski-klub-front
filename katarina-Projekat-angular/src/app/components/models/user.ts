export class User implements IUser {
  id: number;
  password: string;
  roleList: IRole[];
  username: string;
  token: string;
  roles: string[];
}
